from unittest import TestCase

from exceptions.search_service_exceptions import EmailNotFoundException
from main_dependency.main_dependency import get_main_dependency


class TestSearchServiceImpl(TestCase):

    def test_get_phone_by_ukr_net_email(self):
        phone_list = get_main_dependency().get_service_manager().get_search_service().get_phone_list_by_ukr_net_email('opipmash@ukr.net')
        if phone_list:
            self.setUp()
        else:
            self.fail()

    def test_get_phone_by_ukr_net_email_with_not_found(self):
        try:
            search_service = get_main_dependency().get_service_manager().get_search_service()\
                .get_phone_list_by_ukr_net_email('opipmash@ur.net')
        except EmailNotFoundException as error:
            print(error)
            self.setUp()
        else:
            self.fail()

    def test_check_email_by_exits_in_mail_services(self):
        mail_services_result = get_main_dependency().get_service_manager().get_search_service() \
            .check_email_by_exits_in_mail_services('ggerganov@gmail.com')
        if mail_services_result:
            self.setUp()
        else:
            self.fail()
