import abc

from services.interfaces.check_service import CheckService
from services.interfaces.search_service import SearchService


class ServiceManager(abc.ABC):
    @abc.abstractmethod
    def get_check_service(self) -> CheckService:
        pass

    @abc.abstractmethod
    def get_search_service(self) -> SearchService:
        pass
