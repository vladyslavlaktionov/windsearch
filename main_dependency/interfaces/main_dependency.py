import abc

from main_dependency.interfaces.module_manager import ModuleManager
from main_dependency.interfaces.service_manager import ServiceManager


class MainDependency(abc.ABC):
    @abc.abstractmethod
    def get_module_manager(self) -> ModuleManager:
        pass

    @abc.abstractmethod
    def get_service_manager(self) -> ServiceManager:
        pass
