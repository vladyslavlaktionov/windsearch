import abc
from io import TextIOWrapper

from modules.interfaces.company_parser import CompanyParser
from modules.interfaces.config_module import ConfigModule
from modules.interfaces.fake_user_agent_module import FakeUserAgentModule
from modules.interfaces.g_hunt_module import GHuntModule
from modules.interfaces.mail_service_exists_checker_module import MailServiceExistsCheckerModule
from modules.interfaces.telegraph_module import TelegraphModule
from modules.interfaces.ukr_net_api_module import UkrNetApi


class ModuleManager(abc.ABC):

    @abc.abstractmethod
    def get_ukr_net_api(self) -> UkrNetApi:
        pass

    @abc.abstractmethod
    def get_config_module(self) -> ConfigModule:
        pass

    @abc.abstractmethod
    def get_mail_service_exists_checker_module(self) -> MailServiceExistsCheckerModule:
        pass

    @abc.abstractmethod
    def get_fake_user_agent_module(self) -> FakeUserAgentModule:
        pass

    @abc.abstractmethod
    def get_telegraph_module(self) -> TelegraphModule:
        pass

    @abc.abstractmethod
    def get_company_parser_module(self) -> CompanyParser:
        pass

    @abc.abstractmethod
    def get_g_hunt_module(self) -> GHuntModule:
        pass
