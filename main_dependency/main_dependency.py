from main_dependency.interfaces.main_dependency import MainDependency
from main_dependency.interfaces.module_manager import ModuleManager
from main_dependency.interfaces.service_manager import ServiceManager
from main_dependency.module_manager import ModuleManagerImpl
from main_dependency.service_manager import ServiceManagerImpl


class MainDependencyImpl(MainDependency):
    __config: str

    __module_manager: ModuleManager
    __service_manager: ServiceManager

    def __init__(self, config: str):
        self.__config = config

    def __set_module_manager(self, module_manager: ModuleManager) -> None:
        self.__module_manager = module_manager

    def get_module_manager(self) -> ModuleManager:
        if not hasattr(self, '__module_manager'):
            self.__set_module_manager(ModuleManagerImpl(self.__config))
        return self.__module_manager

    def __set_service_manager(self, service_manager: ServiceManager) -> None:
        self.__service_manager = service_manager

    def get_service_manager(self) -> ServiceManager:
        if not hasattr(self, '__module_manager'):
            self.__set_service_manager(ServiceManagerImpl(
                module_manager=self.get_module_manager()
            ))
        return self.__service_manager


def get_main_dependency() -> MainDependency:
    return __main_dependency


__main_dependency: MainDependency = MainDependencyImpl(open('config.json').read())
# __main_dependency: MainDependency = MainDependencyImpl(open('dev.config.json').read())
