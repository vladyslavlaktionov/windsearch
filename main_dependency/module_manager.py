from typing import TextIO

from main_dependency.interfaces.module_manager import ModuleManager
from modules.company_parser.company_parser import CompanyParserImpl
from modules.config_module.config_module_impl import ConfigModuleImpl
from modules.fake_user_agent_module.fake_user_agent_module_impl import FakeUserAgentModuleImpl
from modules.ghunt_module.g_hunt_module import GHuntModuleImpl
from modules.interfaces.company_parser import CompanyParser
from modules.interfaces.config_module import ConfigModule
from modules.interfaces.fake_user_agent_module import FakeUserAgentModule
from modules.interfaces.g_hunt_module import GHuntModule
from modules.interfaces.mail_service_exists_checker_module import MailServiceExistsCheckerModule
from modules.interfaces.telegraph_module import TelegraphModule
from modules.interfaces.ukr_net_api_module import UkrNetApi
from modules.mail_service_exists_checker_module.mail_service_exists_checker_module import \
    MailServiceExistsCheckerModuleImpl
from modules.telegraph_module import TelegraphModuleImpl
from modules.ukr_net_api_module.ukr_net_api_module_impl import UkrNetApiImpl


class ModuleManagerImpl(ModuleManager):
    __config: str

    __ukr_net_api: UkrNetApi
    __mail_service_exists_checker_module: MailServiceExistsCheckerModule
    __fake_user_agent_module: FakeUserAgentModule
    __company_parser_module: CompanyParser
    __telegraph_module: TelegraphModule
    __g_hunt_module: GHuntModule
    __config_module: ConfigModule

    def __init__(self, config: str):
        self.__config = config

    def get_ukr_net_api(self) -> UkrNetApi:
        if not hasattr(self, '__ukr_net_api'):
            self.__set_ukr_net_api(UkrNetApiImpl())
        return self.__ukr_net_api

    def __set_ukr_net_api(self, ukr_net_api: UkrNetApi) -> None:
        self.__ukr_net_api = ukr_net_api

    def get_config_module(self) -> ConfigModule:
        if not hasattr(self, '__config_module'):
            self.__set_config_module(ConfigModuleImpl(self.__config))
        return self.__config_module

    def __set_config_module(self, config_module: ConfigModule) -> None:
        self.__config_module = config_module

    def get_mail_service_exists_checker_module(self) -> MailServiceExistsCheckerModule:
        if not hasattr(self, '__mail_service_exists_checker_module'):
            self.__set_mail_service_exists_checker_module(MailServiceExistsCheckerModuleImpl())
        return self.__mail_service_exists_checker_module

    def __set_mail_service_exists_checker_module(self,
                                                 mail_service_exists_checker_module: MailServiceExistsCheckerModule) -> None:
        self.__mail_service_exists_checker_module = mail_service_exists_checker_module

    def get_fake_user_agent_module(self) -> FakeUserAgentModule:
        if not hasattr(self, '__fake_user_agent_module'):
            self.__set_fake_user_agent_module(FakeUserAgentModuleImpl())
        return self.__fake_user_agent_module

    def __set_fake_user_agent_module(self, fake_user_agent_module: FakeUserAgentModule) -> None:
        self.__fake_user_agent_module = fake_user_agent_module

    def get_telegraph_module(self) -> TelegraphModule:
        if not hasattr(self, '__telegraph_module'):
            self.__set_telegraph_module(TelegraphModuleImpl())
        return self.__telegraph_module

    def __set_telegraph_module(self, telegraph_module: TelegraphModule) -> None:
        self.__telegraph_module = telegraph_module

    def get_company_parser_module(self) -> CompanyParser:
        if not hasattr(self, '__company_parser_module'):
            self.__set_company_parser_module(CompanyParserImpl(
                self.get_config_module().get_company_parser_config()
            ))
        return self.__company_parser_module

    def __set_company_parser_module(self, company_parser_module: CompanyParser) -> None:
        self.__company_parser_module = company_parser_module

    def get_g_hunt_module(self) -> GHuntModule:
        if not hasattr(self, '__g_hunt_module'):
            self.__set_g_hunt_module(GHuntModuleImpl(self.get_config_module().get_g_hunt_module_config()))
        return self.__g_hunt_module

    def __set_g_hunt_module(self, g_hunt_module: GHuntModule):
        self.__g_hunt_module = g_hunt_module
