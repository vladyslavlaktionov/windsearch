from main_dependency.interfaces.module_manager import ModuleManager
from main_dependency.interfaces.service_manager import ServiceManager
from services.check_service import CheckServiceImpl
from services.interfaces.check_service import CheckService

from services.interfaces.search_service import SearchService
from services.search_service.search_service_impl import SearchServiceImpl


class ServiceManagerImpl(ServiceManager):
    __module_manager: ModuleManager

    __search_service: SearchService
    __check_service: CheckService

    def __init__(self, module_manager: ModuleManager):
        self.__module_manager = module_manager

    def __set_check_service(self, check_service: CheckService) -> None:
        self.__check_service = check_service

    def get_check_service(self) -> CheckService:
        if not hasattr(self, '__check_service'):
            self.__set_check_service(
                CheckServiceImpl(
                    self.__module_manager
                )
            )
        return self.__check_service

    def __set_search_service(self, search_service: SearchService) -> None:
        self.__search_service = search_service

    def get_search_service(self) -> SearchService:
        if not hasattr(self, '__search_service'):
            self.__set_search_service(
                SearchServiceImpl(
                    self.__module_manager
                )
            )
        return self.__search_service
