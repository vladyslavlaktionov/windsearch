class RateLimitException(Exception):
    message = 'Rate limit exception'


class UserNotFoundException(Exception):
    message = 'User with this email not found'
