from typing import List
import requests

from modules.interfaces.ukr_net_api_module import UkrNetApi
from modules.ukr_net_api_module.exceptions import UserNotFoundException, RateLimitException


class UkrNetApiImpl(UkrNetApi):
    def get_request_id(self) -> str:
        response = requests.post('https://accounts.ukr.net/api/v1/cai/browser/get')
        if response.status_code == 200:
            return response.json().get('id')
        raise RateLimitException

    def get_recovery_session_id(self, email: str, request_id: str) -> str:
        data = {
            "user": email,
            "client_application_id": request_id
        }
        response = requests.post('https://accounts.ukr.net/api/v2/recovery/init', json=data)
        if response.status_code == 200:
            return response.json().get('id')
        if response.status_code == 599:
            raise UserNotFoundException
        raise RateLimitException

    def get_recovery_info(self, recovery_session_id: str) -> List[str]:
        headers = {
            'X-Rec-Sid': recovery_session_id,
        }
        response = requests.post('https://accounts.ukr.net/api/v2/recovery/next',
                                 headers=headers)
        if response.status_code == 200:
            return [contact.get('value') for contact in response.json().get('contacts')]
        raise UserNotFoundException
