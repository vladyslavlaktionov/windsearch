from dataclasses import field, dataclass
from typing import List


@dataclass
class MailRuResult:
    phones: List[str] = field(default_factory=list)
    emails: List[str] = field(default_factory=list)
