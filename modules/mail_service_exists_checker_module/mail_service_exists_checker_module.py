import re
from datetime import datetime
from typing import Optional

import requests
from bs4 import BeautifulSoup

from modules.interfaces.mail_service_exists_checker_module import MailServiceExistsCheckerModule
from modules.mail_service_exists_checker_module.models import MailRuResult


class MailServiceExistsCheckerModuleImpl(MailServiceExistsCheckerModule):

    def check_google(self, email: str, user_agent: str) -> Optional[str]:
        headers = {
            'User-Agent': user_agent,
            'Accept': '*/*',
            'Accept-Language': 'en,en-US;q=0.5',
            'X-Same-Domain': '1',
            'Google-Accounts-XSRF': '1',
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
            'Origin': 'https://accounts.google.com',
            'DNT': '1',
            'Connection': 'keep-alive',
            'Referer': 'https://accounts.google.com/signup/v2/webcreateaccount?continue=https%3A%2F%2Faccounts.google.com%2F&gmb=exp&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp',
            'TE': 'Trailers',
        }
        try:
            url = "https://accounts.google.com/signup/v2/webcreateaccount?continue=https%3A%2F%2Faccounts.google.com" \
                  "%2FManageAccount%3Fnc%3D1&gmb=exp&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp"
            response = requests.get(
                url,
                headers=headers
            )

            freq = response.text.split('quot;,null,null,null,&quot;')[1].split('&quot')[0]

            params = {
                'hl': 'fr',
                'rt': 'j',
            }

            data = {
                'continue': 'https://accounts.google.com/',
                'dsh': '',
                'hl': 'fr',
                'f.req': '["' + freq + '","","","' + email + '",false]',
                'azt': '',
                'cookiesDisabled': 'false',
                'deviceinfo': '[null,null,null,[],null,"FR",null,null,[],"GlifWebSignIn",null,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[],null,null,null,[],[]],null,null,null,null,0,null,false]',
                'gmscoreversion': 'unined',
                '': ''

            }
            response = requests.post(
                'https://accounts.google.com/_/signup/webusernameavailability',
                params=params,
                headers=headers,
                data=data
            )
            if '"gf.wuar",2' in response.text:
                login, domain = email.split('@')
                return login + "@gmail.com"
        except Exception:
            pass

    def check_laposte(self, email: str, user_agent: str) -> bool:
        try:
            data = {
                'email': email,
                'customerId': '',
                'tunnelSteps': ''
            }
            response = requests.post('https://www.laposte.fr/authentification', data=data)
            post_soup = BeautifulSoup(response.content, 'html.parser')
            wrong_email = post_soup.find_all('span', id="wrongEmail")
            return wrong_email != []
        except Exception:
            return False

    def check_yahoo(self, email: str, user_agent: str) -> bool:
        try:
            headers = {
                'User-Agent': user_agent,
            }
            req = requests.get("https://login.yahoo.com", headers=headers)

            headers = {
                'User-Agent': user_agent,
            }

            params = {
                '.src': 'fpctx',
                '.intl': 'ca',
                '.lang': 'en-CA',
                '.done': 'https://ca.yahoo.com',
            }
            data = {
                'acrumb': req.text.split('<input type="hidden" name="acrumb" value="')[1].split('"')[0],
                'sessionIndex': req.text.split('<input type="hidden" name="sessionIndex" value="')[1].split('"')[0],
                'username': email,
                'passwd': '',
                'signin': 'Next',
                'persistent': 'y'}

            response = requests.post(
                'https://login.yahoo.com/',
                headers=headers,
                params=params,
                data=data)
            response = response.json()
            if "error" in response.keys() and not response["error"]:
                return True
            elif "render" in response.keys() and response["render"]["error"] == "messages.ERROR_INVALID_USERNAME":
                return False
        except Exception:
            return False

    def check_protonmail(self, email: str) -> Optional[datetime]:
        try:
            response = requests.get('https://api.protonmail.ch/pks/lookup?op=index&search=' + email)
            if "info:1:1" in response.text:
                first_pattern = "2048:(.*)::"  # RSA 2048-bit (Older but faster)
                second_pattern = "4096:(.*)::"  # RSA 4096-bit (Secure but slow)
                third_pattern = "22::(.*)::"  # X25519 (Modern, fastest, secure)
                try:
                    timestamp = int(re.search(first_pattern, response.text).group(1))
                except:
                    try:
                        timestamp = int(re.search(second_pattern, response.text).group(1))
                    except:
                        timestamp = int(re.search(third_pattern, response.text).group(1))
                date = datetime.fromtimestamp(timestamp)
                return date
        except:
            pass

    def check_mail_ru(self, email: str, user_agent: str) -> Optional[MailRuResult]:
        try:
            headers = {
                'authority': 'account.mail.ru',
                'accept': 'application/json, text/javascript, */*; q=0.01',
                'x-requested-with': 'XMLHttpRequest',
                'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'origin': 'https://account.mail.ru',
                'sec-fetch-site': 'same-origin',
                'sec-fetch-mode': 'cors',
                'sec-fetch-dest': 'empty',
                'referer': f'https://account.mail.ru/recovery?email={email}',
                'user-agent': user_agent,
                'accept-language': 'ru',
            }
            data = f'email={email}&htmlencoded=false'.replace('@', '%40')
            response = requests.post(
                'https://account.mail.ru/api/v1/user/password/restore',
                headers=headers,
                data=data)
            if response.status_code == 200:
                json_data = response.json()
                if json_data.get('status') == 200 and json_data.get('body'):
                    body = json_data.get('body')
                    phones = [phone for phone in (body.get('phones') or [])]
                    emails = [email for email in (body.get('emails') or [])]
                    if phones or phones:
                        return MailRuResult(phones=phones, emails=emails)
        except Exception as error:
            print(error)