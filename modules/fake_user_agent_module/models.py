from enum import Enum


class BrowserModel(str, Enum):
    CHROME = "chrome"
    OPERA = 'opera'
    FIREFOX = 'firefox'
    INTERNET_EXPLORER = 'internetexplorer'
    SAFARI = 'safari'
