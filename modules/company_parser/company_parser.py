import re
from typing import List, Optional

import html2text
import requests
from bs4 import BeautifulSoup

from modules.company_parser.models import Company, SearchTransactionResult, SearchCompanyResult, CompanyParserConfig
from modules.interfaces.company_parser import CompanyParser


class CompanyParserImpl(CompanyParser):
    access_token: str = None

    def __init__(self, company_parser_config: CompanyParserConfig):
        self.update_access_token(company_parser_config.username, company_parser_config.password)

    def update_access_token(self, username: str, password: str) -> int:
        data = dict(
            signusername=username,
            signpassword=password
        )
        response = requests.post('https://en.52wmb.com/signin', data=data)
        if response.status_code == 200:
            data = response.json().get('data').get('auth_access')
            self.access_token = data.get('auth_access_token')
            return int(data.get('expires'))

    def search_company(self, search_query: str, page: Optional[int] = 1) -> SearchCompanyResult:
        company_list: List[Company] = []
        cookies = {
            'access_token': self.access_token,
        }
        response = requests.get(
            'https://en.52wmb.com/async/company',
            params=dict(
                key=search_query,
                page=page,
                st=1,
                counts=""
            ),
            cookies=cookies
        )
        beautiful_soup = BeautifulSoup(response.text, 'html.parser')
        for company in beautiful_soup.find_all(class_="ss-Cname company-detail-a"):
            company_list.append(Company(
                id=int(company['href'][7:]),
                name=company.text
            ))
        raw_page = (re.findall(r'this\.value > \d+', response.text) or [None])[0]
        count_max = int(str(raw_page).replace('this.value > ', "")) if raw_page else None

        return SearchCompanyResult(
            company_list=company_list,
            page_count=count_max,
            previous=page - 1 if page - 1 > 0 else None,
            next_page=page + 1 if count_max and page + 1 <= count_max else None,
        )

    def get_company_transaction_info(self, company_id: int, page=1) -> SearchTransactionResult:
        cookies = {
            'access_token': self.access_token,
        }
        params = (
            ('start', company_id),
        )
        data = {
            'page': page
        }
        response = requests.post('https://en.52wmb.com/async/trade/country/bill/page', params=params,
                                 cookies=cookies, data=data)
        beautiful_soup = BeautifulSoup(response.text, 'html.parser')
        transaction_info = html2text.html2text(
            str(beautiful_soup.find('table'))
        ).replace('/buyer/', 'https://en.52wmb.com/buyer/')
        transaction_info = transaction_info.replace('/supplier/', 'https://en.52wmb.com/supplier/')
        return SearchTransactionResult(
            transaction_info=transaction_info,
            previous=page - 1 if "previous feat-previous" in response.text else None,
            next_page=page + 1 if "next feat-next" in response.text else None,
        )
