from dataclasses import dataclass, field
from typing import Optional, List


@dataclass
class CompanyParserConfig:
    username: str
    password: str


@dataclass
class Company:
    id: int
    name: str


@dataclass
class SearchCompanyResult:
    next_page: Optional[int] = None
    previous: Optional[int] = None
    page_count: Optional[int] = None
    company_list: List[Company] = field(default_factory=list)


@dataclass
class SearchTransactionResult:
    transaction_info: str
    next_page: Optional[int] = None
    previous: Optional[int] = None


@dataclass
class Transaction:
    company_id: int
    buyer: str
    supplier: str
    trade_date: str
    purchase: str
    hs_code: str
    product: str
    weight: str
    container_qty: str
    total_amount: str
    unit_price: str
    origin: Optional[str] = None
