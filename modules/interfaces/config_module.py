import abc

from modules.config_module.models import GHuntModuleConfig, GlobalConfig, CompanyParserConfig


class ConfigModule(abc.ABC):
    @abc.abstractmethod
    def get_g_hunt_module_config(self) -> GHuntModuleConfig:
        pass

    @abc.abstractmethod
    def get_company_parser_config(self) -> CompanyParserConfig:
        pass

    @abc.abstractmethod
    def get_global_config(self) -> GlobalConfig:
        pass
