import abc

from modules.fake_user_agent_module.models import BrowserModel


class FakeUserAgentModule(abc.ABC):
    @abc.abstractmethod
    def get_fake_user_agent(self, browser: BrowserModel) -> str:
        pass
