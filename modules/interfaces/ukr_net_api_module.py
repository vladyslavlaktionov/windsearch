import abc
from typing import List


class UkrNetApi(abc.ABC):
    @abc.abstractmethod
    def get_request_id(self) -> str:
        pass

    @abc.abstractmethod
    def get_recovery_session_id(self, email: str, request_id: str) -> str:
        pass

    @abc.abstractmethod
    def get_recovery_info(self, recovery_session_id: str) -> List[str]:
        pass
