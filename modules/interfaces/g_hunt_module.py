import abc
from typing import List

from modules.ghunt_module.models import AccountData, YoutubeChannel, GoogleDocsSearchResult, CalendarEvent, \
    GmailInfoResultModel


class GHuntModule(abc.ABC):
    @abc.abstractmethod
    def get_gmail_info_list_by_email(self, email: str) -> List[GmailInfoResultModel]:
        pass

    @abc.abstractmethod
    def get_gmail_account_data(self, account_id: str) -> AccountData:
        pass

    @abc.abstractmethod
    def global_search_youtube_channels(self, name: str) -> List[YoutubeChannel]:
        pass

    @abc.abstractmethod
    def search_youtube_channels_in_google_docs(self, search_query: str) -> List[YoutubeChannel]:
        pass

    @abc.abstractmethod
    def google_docs_search(self, query, size=1000) -> List[GoogleDocsSearchResult]:
        pass

    @abc.abstractmethod
    def search_youtube_channel_in_youtube(self, search_query: str) -> List[YoutubeChannel]:
        pass

    @abc.abstractmethod
    def get_google_calendar_events(self, email: str) -> List[CalendarEvent]: pass
