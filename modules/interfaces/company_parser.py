import abc

from modules.company_parser.models import SearchTransactionResult, SearchCompanyResult


class CompanyParser(abc.ABC):
    @abc.abstractmethod
    def update_access_token(self, username: str, password: str) -> int:
        pass

    @abc.abstractmethod
    def search_company(self, search_query: str) -> SearchCompanyResult:
        pass

    @abc.abstractmethod
    def get_company_transaction_info(self, company_id: int, page=1) -> SearchTransactionResult:
        pass
