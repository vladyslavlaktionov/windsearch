import abc
from datetime import datetime
from typing import Optional

from modules.mail_service_exists_checker_module.models import MailRuResult


class MailServiceExistsCheckerModule(abc.ABC):
    @abc.abstractmethod
    def check_google(self, email: str, user_agent: str) -> Optional[str]:
        pass

    @abc.abstractmethod
    def check_laposte(self, email: str, user_agent: str) -> bool:
        pass

    @abc.abstractmethod
    def check_yahoo(self, email: str, user_agent: str) -> bool:
        pass

    @abc.abstractmethod
    def check_protonmail(self, email: str) -> Optional[datetime]:
        pass

    @abc.abstractmethod
    def check_mail_ru(self, email: str, user_agent: str) -> Optional[MailRuResult]:
        pass
