import abc


class TelegraphModule(abc.ABC):
    @abc.abstractmethod
    def create_post(self, title: str, author: str, text: str, author_url: str) -> str:
        pass

