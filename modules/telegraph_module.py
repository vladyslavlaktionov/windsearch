from html_telegraph_poster import TelegraphPoster

from modules.interfaces.telegraph_module import TelegraphModule


class TelegraphModuleImpl(TelegraphModule):
    telegraph_poster: TelegraphPoster

    def __init__(self):
        self.telegraph_poster = TelegraphPoster(use_api=True)
        self.telegraph_poster.create_api_token('Elon Musk', 'Elon', 'https://www.spacex.com/')

    def create_post(self, title: str, author: str, text: str, author_url: str) -> str:
        return self.telegraph_poster.post(title=title, author=author, text=text, author_url=author_url).get('url')

