import json
import re
from datetime import datetime, timezone
from io import BytesIO
from typing import List, Optional
from urllib.parse import urlencode

import imagehash
import requests
from PIL import Image
from dateutil.relativedelta import relativedelta
from geopy.geocoders import Nominatim
from requests import Session

from modules.ghunt_module.exceptions import EmailAddressDoesNotBelongToAGoogleAccountException
from modules.ghunt_module.models import AccountData, CalendarEvent, GmailInfoResultModel, YoutubeChannel, \
    GoogleDocsSearchResult, GHuntModuleConfig
from modules.interfaces.g_hunt_module import GHuntModule


class GHuntModuleImpl(GHuntModule):
    def __init__(self, config: GHuntModuleConfig):
        self.config: GHuntModuleConfig = config
        self.session = Session()
        self.session.cookies = requests.utils.cookiejar_from_dict(config.cookies)
        self.session.headers = config.headers

    def get_gmail_info_list_by_email(self, email: str) -> List[GmailInfoResultModel]:
        if not email:
            exit("Please give a valid email.\nExample : larry@google.com")
        gmail_info_result_model_list: List[GmailInfoResultModel] = []

        email_google_account_data = self.get_email_google_account_data(email)

        geolocator = Nominatim(user_agent="nominatim")

        for user in email_google_account_data["matches"]:
            account_id = int(user["personId"][0])
            email = user["lookupId"]
            infos = email_google_account_data["people"][str(account_id)]

            # get name & profile picture
            account_data = self.get_gmail_account_data(account_id)
            if not account_data.name:
                for name in infos["name"]:
                    if 'displayName' in name.keys():
                        account_data.name = name["displayName"]
            if account_data.profile_pic_url:
                img = Image.open(BytesIO(self.session.get(account_data.profile_pic_url).content))
                profile_pic_hash = self.__get_image_hash(img)
                is_default_profile_pic = self.__detect_default_profile_pic(profile_pic_hash)
                if is_default_profile_pic:
                    account_data.profile_pic_url = None

            timestamp = int(infos["metadata"]["lastUpdateTimeMicros"][:-3])
            last_edit = datetime.utcfromtimestamp(timestamp)

            if "extendedData" in infos:
                is_bot = infos["extendedData"]["hangoutsExtendedData"]["isBot"]
                if is_bot:
                    is_bot = True
                else:
                    is_bot = False
            else:
                is_bot = None

            ytb_hunt = False
            try:
                services = [x["appType"].lower() if x["appType"].lower() != "babel" else "hangouts" for x in
                            infos["inAppReachability"]]
                if account_data.name and (self.config.ytb_hunt_always or "youtube" in services):
                    ytb_hunt = True
            except KeyError:
                ytb_hunt = True
                services = []
            youtube_channel_list = self.global_search_youtube_channels(account_data.name)

            # check YouTube
            # if account_data.name and ytb_hunt:
            #     confidence = None
            #
            #     if youtube_channel_list:
            #         confidence, channels = self.\
            #             __get_youtube_confidence(youtube_channel_list, account_data)
            #
            #         if confidence:
            #             print(f"\n[+] YouTube channel (confidence => {confidence}%) :")
            #             for channel in channels:
            #                 print(f"- [{channel['name']}] {channel['profile_url']}")
            #             possible_usernames = ytb.extract_usernames(channels)
            #             if possible_usernames:
            #                 print("\n[+] Possible usernames found :")
            #                 for username in possible_usernames:
            #                     print(f"- {username}")
            #         else:
            #             print("\n[-] YouTube channel not found.")

            # reviews = gmaps.scrape(gaiaID, client, cookies, config, config.headers, config.regexs["review_loc_by_id"],
            #                        config.headless)
            #
            # if reviews:
            #     confidence, locations = gmaps.get_confidence(geolocator, reviews, config.gmaps_radius)
            #     print(f"\n[+] Probable location (confidence => {confidence}) :")
            #
            #     loc_names = []
            #     for loc in locations:
            #         loc_names.append(
            #             f"- {loc['avg']['town']}, {loc['avg']['country']}"
            #         )
            #
            #     loc_names = set(loc_names)  # delete duplicates
            #     for loc in loc_names:
            #         print(loc)

            calendar_events = self.get_google_calendar_events(email)
            gmail_info_result_model_list.append(GmailInfoResultModel(
                account_data=account_data,
                calendar_events=calendar_events,
                is_bot=is_bot,
                last_edit=last_edit,
                services=services,
                youtube_channels=self.global_search_youtube_channels(account_data.name)
            ))
        return gmail_info_result_model_list

    def get_google_calendar_events(self, email: str) -> List[CalendarEvent]:
        calendar_response = self.__calendar_fetch(email)
        calendar_events: List[CalendarEvent] = []
        if calendar_response:
            events = calendar_response["events"]
            calendar_events = self.__get_calendar_events(events)
        return calendar_events

    def __calendar_fetch(self, email: str):
        url_endpoint = f"https://calendar.google.com/calendar/u/0/embed?src={email}"
        req = requests.get(url_endpoint, params=dict(hl='en'),
                           cookies=requests.utils.cookiejar_from_dict(
                               {"CONSENT": self.config.default_consent_cookie}
                           )
                           )
        source = req.text
        try:
            calendar_id = source.split('title\":\"')[1].split('\"')[0]
            single_events = "true"
            max_attendees = 1
            max_results = 250
            time_min = datetime.strptime(source.split('preloadStart\":\"')[1].split('\"')[0], '%Y%m%d').replace(
                tzinfo=timezone.utc).isoformat()
            api_key = source.split('developerKey\":\"')[1].split('\"')[0]
        except IndexError:
            return False

        json_calendar_endpoint = self.__assemble_api_req(
            calendar_id,
            single_events,
            max_attendees,
            max_results,
            time_min,
            api_key,
            email
        )
        req = self.session.get(json_calendar_endpoint)
        data = json.loads(req.text)
        events = []
        try:
            for item in data["items"]:
                title = item["summary"]
                start = self.__get_datetime_utc(item["start"]["dateTime"])
                end = self.__get_datetime_utc(item["end"]["dateTime"])

                events.append({"title": title, "start": start, "end": end})
        except KeyError:
            return False

        return {"status": "available", "events": events}

    @staticmethod
    def __assemble_api_req(calendar_id, single_events, max_attendees, max_results, time_min, api_key, email):
        base_url = f"https://clients6.google.com/calendar/v3/calendars/{email}/events?"
        params = {
            "calendarId": calendar_id,
            "singleEvents": single_events,
            "maxAttendees": max_attendees,
            "maxResults": max_results,
            "timeMin": time_min,
            "key": api_key
        }
        base_url += urlencode(params, doseq=True)
        return base_url

    @staticmethod
    def __get_datetime_utc(date_str):
        date = datetime.fromisoformat(date_str)
        margin = date.utcoffset()
        return date.replace(tzinfo=timezone.utc) - margin

    def get_email_google_account_data(self, email: str):
        url = "https://people-pa.clients6.google.com/v2/people/lookup?key={}".format(self.config.hangouts_token)

        body = """id={}&type=EMAIL&matchType=EXACT&extensionSet.extensionNames=HANGOUTS_ADDITIONAL_DATA&extensionSet.extensionNames=HANGOUTS_OFF_NETWORK_GAIA_LOOKUP&extensionSet.extensionNames=HANGOUTS_PHONE_DATA&coreIdParams.useRealtimeNotificationExpandedAcls=true&requestMask.includeField.paths=person.email&requestMask.includeField.paths=person.gender&requestMask.includeField.paths=person.in_app_reachability&requestMask.includeField.paths=person.metadata&requestMask.includeField.paths=person.name&requestMask.includeField.paths=person.phone&requestMask.includeField.paths=person.photo&requestMask.includeField.paths=person.read_only_profile_info&requestMask.includeContainer=AFFINITY&requestMask.includeContainer=PROFILE&requestMask.includeContainer=DOMAIN_PROFILE&requestMask.includeContainer=ACCOUNT&requestMask.includeContainer=EXTERNAL_ACCOUNT&requestMask.includeContainer=CIRCLE&requestMask.includeContainer=DOMAIN_CONTACT&requestMask.includeContainer=DEVICE_CONTACT&requestMask.includeContainer=GOOGLE_GROUP&requestMask.includeContainer=CONTACT""".format(
            email)

        headers = {
            "X-HTTP-Method-Override": "GET",
            "Authorization": self.config.hangouts_api_authorization,
            "Content-Type": "application/x-www-form-urlencoded",
            "Origin": "https://hangouts.google.com"
        }

        response = self.session.post(url, data=body, headers=headers)

        data = response.json()
        if "matches" not in data:
            raise EmailAddressDoesNotBelongToAGoogleAccountException
        return data

    @staticmethod
    def __get_calendar_events(events) -> List[CalendarEvent]:
        calendar_events: List[CalendarEvent] = []
        limit = 5
        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        after = [date for date in events if date["start"] >= now][:limit]
        before = [date for date in events if date["start"] <= now][:limit]
        target = after if after else before

        for event in target:
            calendar_events.append(
                CalendarEvent(
                    title=event["title"],
                    date=event["start"].strftime("%Y/%m/%d %H:%M:%S"),
                    duration=relativedelta(event["end"], event["start"])
                )
            )
        return calendar_events

    def get_gmail_account_data(self, account_id: int) -> AccountData:
        url = f"https://people-pa.clients6.google.com/v2/people?person_id={account_id}&request_mask.include_container=PROFILE&request_mask.include_container=DOMAIN_PROFILE&request_mask.include_field.paths=person.metadata.best_display_name&request_mask.include_field.paths=person.photo&request_mask.include_field.paths=person.email&core_id_params.enable_private_names=true&key={self.config.internal_api_token}"
        response = self.session.get(url, headers={
            **self.config.headers,
            "Origin": "https://drive.google.com",
            "authorization": self.config.internal_api_authorization,
            "Host": "people-pa.clients6.google.com"
        })
        response_json = response.json()
        try:
            name = response.json()["personResponse"][0]["person"]["metadata"]["bestDisplayName"]["displayName"]
        except KeyError:
            name = self.__get_gmail_account_name(account_id)
        try:
            profile_pic_url = response_json["personResponse"][0]["person"]["photo"][0]["url"]
        except KeyError:
            profile_pic_url = None

        return AccountData(name=name, profile_pic_url=profile_pic_url, account_id=account_id)

    def __get_gmail_account_name(self, account_id: int) -> Optional[str]:
        response = self.session.get("https://www.google.com/maps/contrib/{}".format(account_id))
        match = re.search(r'<meta content="Contributions by (.*?)" itemprop="name">', response.text)
        if not match:
            return None
        return match[1]

    @staticmethod
    def __get_image_hash(img):
        return str(imagehash.average_hash(img))

    @staticmethod
    def __detect_default_profile_pic(profile_pic_hash: str) -> bool:
        return profile_pic_hash == '00001818183c7e7e'

    def global_search_youtube_channels(self, name: str) -> List[YoutubeChannel]:
        from_youtube = self.search_youtube_channel_in_youtube(name)
        from_google_docs = self.search_youtube_channels_in_google_docs(name)
        return [*from_youtube, *from_google_docs]

    def search_youtube_channel_in_youtube(self, search_query: str) -> List[YoutubeChannel]:
        try:
            response = self.session.get("https://www.youtube.com/results", params=dict(
                search_query=search_query,
                sp="EgIQAg%253D%253D"
            ))
            source = response.text
            data = json.loads(
                source.split('window["ytInitialData"] = ')[1].split('window["ytInitialPlayerResponse"]')[0].split(
                    ';\n')[0])
            channels = \
                data["contents"]["twoColumnSearchResultsRenderer"]["primaryContents"]["sectionListRenderer"][
                    "contents"][0][
                    "itemSectionRenderer"]["contents"]
            results: List[YoutubeChannel] = []
            for channel in channels:
                if len(results) >= 10:
                    break
                title = channel["channelRenderer"]["title"]["simpleText"]
                if not search_query.lower() in title.lower():
                    continue
                avatar_link = channel["channelRenderer"]["thumbnail"]["thumbnails"][0]["url"].split('=')[0]
                if avatar_link[:2] == "//":
                    avatar_link = "https:" + avatar_link
                profile_url = "https://youtube.com" + \
                              channel["channelRenderer"]["navigationEndpoint"]["browseEndpoint"][
                                  "canonicalBaseUrl"]
                results.append(YoutubeChannel(
                    profile_url=profile_url,
                    name=title,
                    avatar_link=avatar_link,
                    username=self.get_youtube_channel_username(profile_url) if profile_url else None
                ))
            return results
        except (KeyError, IndexError):
            return []

    def search_youtube_channels_in_google_docs(self, search_query: str) -> List[YoutubeChannel]:
        search_query = f"site:youtube.com/channel \\\"{search_query}\\\""
        search_results = self.google_docs_search(search_query)
        results: List[YoutubeChannel] = []
        channels = []
        for result in search_results:
            sanitized = "https://youtube.com/" + ('/'.join(result.link.split('/')[3:5]))
            if sanitized not in channels:
                channels.append(sanitized)

        if not channels:
            return []

        channels = channels[:5]

        for profile_url in channels:
            data = None
            avatar_link = None

            retries = 2
            for retry in range(retries):
                req = self.session.get(profile_url)
                source = req.text
                try:
                    data = json.loads(
                        source.split('var ytInitialData = ')[1].split(';</script>')[0])
                    avatar_link = \
                        data["metadata"]["channelMetadataRenderer"]["avatar"]["thumbnails"][0]["url"].split('=')[0]
                except (KeyError, IndexError) as e:
                    if retry == retries:
                        return []
                    continue
                else:
                    break
            title = data["metadata"]["channelMetadataRenderer"]["title"]
            results.append(YoutubeChannel(
                profile_url=profile_url,
                name=title,
                avatar_link=avatar_link,
                username=self.get_youtube_channel_username(profile_url) if profile_url else None
            ))
        return results

    def google_docs_search(self, query, size=1000) -> List[GoogleDocsSearchResult]:
        data = {"request": '["documentsuggest.search.search_request","{}",[{}],null,1]'.format(query, size)}
        req = self.session.post(
            'https://docs.google.com/document/d/{}/explore/search?token={}'.format(
                self.config.google_docs_public_doc, self.config.google_docs_token,
            ),
            data=data)
        if req.status_code != 200:
            raise Exception("Error (GDocs): request gives {}".format(req.status_code))

        output = json.loads(req.text.replace(")]}'", ""))
        if isinstance(output[0][1], str) and output[0][1].lower() == "xsrf":
            raise Exception(
                "It means your cookies have expired, please generate new ones."
            )

        results: List[GoogleDocsSearchResult] = []
        for result in output[0][1]:
            link = result[0][0]
            title = result[0][1]
            desc = result[0][2]

            results.append(GoogleDocsSearchResult(
                title=title,
                desc=desc,
                link=link
            ))

        return results

    @staticmethod
    def __get_youtube_confidence(youtube_channel_list: List[YoutubeChannel], account_data: AccountData):
        score_steps = 4

        for index, channel in enumerate(youtube_channel_list):
            for channel_nb, channel in enumerate(source["channels"]):
                score = 0

                if hash == channel["hash"]:
                    score += score_steps * 4
                if query == channel["name"]:
                    score += score_steps * 3
                if query in channel["name"]:
                    score += score_steps * 2
                if ((source["origin"] == "youtube" and source["length"] <= 5) or
                        (source["origin"] == "google" and source["length"] <= 4)):
                    score += score_steps
                data[source_nb]["channels"][channel_nb]["score"] = score

        channels = []
        for source in data:
            for channel in source["channels"]:
                found_better = False
                for source2 in data:
                    for channel2 in source["channels"]:
                        if channel["profile_url"] == channel2["profile_url"]:
                            if channel2["score"] > channel["score"]:
                                found_better = True
                                break
                    if found_better:
                        break
                if found_better:
                    continue
                else:
                    channels.append(channel)
        channels = sorted([json.loads(chan) for chan in set([json.dumps(channel) for channel in channels])],
                          key=lambda k: k['score'], reverse=True)
        panels = sorted(set([c["score"] for c in channels]), reverse=True)
        if not channels or (panels and panels[0] <= 0):
            return 0, []

        maxscore = sum([p * score_steps for p in range(1, score_steps + 1)])
        for panel in panels:
            chans = [c for c in channels if c["score"] == panel]
            if len(chans) > 1:
                panel -= 5
            return (panel / maxscore * 100), chans

    @staticmethod
    def get_youtube_channel_username(profile_url: str) -> Optional[str]:
        if "/user/" in profile_url:
            return profile_url.split("/user/")[1]
