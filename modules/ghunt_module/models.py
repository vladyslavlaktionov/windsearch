from dataclasses import dataclass, field
from datetime import datetime
from typing import Optional, List

from dateutil.relativedelta import relativedelta



@dataclass
class GHuntModuleConfig:
    cookies: dict
    headers: dict
    google_docs_token: str
    internal_api_token: str
    internal_api_authorization: str
    hangouts_api_authorization: str
    hangouts_token: str
    default_consent_cookie: str = "YES+FR.fr+V10+BX"
    google_docs_public_doc: str = "1jaEEHZL32t1RUN5WuZEnFpqiEPf_APYKrRBG9LhLdvE"
    ytb_hunt_always: bool = False


@dataclass
class CalendarEvent:
    title: str
    date: str
    duration: relativedelta


@dataclass
class AccountData:
    account_id: int
    name: str
    profile_pic_url: Optional[str]


@dataclass
class YoutubeChannel:
    profile_url: str
    name: str
    avatar_link: str
    username: Optional[str] = None


@dataclass
class GmailInfoResultModel:
    account_data: AccountData
    calendar_events: List[CalendarEvent]
    services: List[str] = field(default_factory=list)
    youtube_channels: List[YoutubeChannel] = field(default_factory=list)
    is_bot: Optional[bool] = None
    last_edit: Optional[datetime] = None


@dataclass
class GoogleDocsSearchResult:
    title: str
    desc: str
    link: str
