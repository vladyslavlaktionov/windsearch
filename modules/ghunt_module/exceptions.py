class EmailAddressDoesNotBelongToAGoogleAccountException(Exception):
    message = "[-] This email address does not belong to a Google Account."
