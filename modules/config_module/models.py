from dataclasses import dataclass, field


@dataclass
class GHuntModuleConfig:
    cookies: dict
    headers: dict
    google_docs_token: str
    internal_api_token: str
    internal_api_authorization: str
    hangouts_api_authorization: str
    hangouts_token: str
    default_consent_cookie: str = field(default="YES+FR.fr+V10+BX")
    google_docs_public_doc: str = field(default="1jaEEHZL32t1RUN5WuZEnFpqiEPf_APYKrRBG9LhLdvE")
    ytb_hunt_always: bool = False


@dataclass
class CompanyParserConfig:
    username: str
    password: str


@dataclass
class GlobalConfig:
    g_hunt_module_config: GHuntModuleConfig
    company_parser_config: CompanyParserConfig
