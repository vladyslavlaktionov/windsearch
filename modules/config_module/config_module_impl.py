import json
from typing import TextIO

from modules.config_module.models import GHuntModuleConfig, GlobalConfig, CompanyParserConfig
from modules.interfaces.config_module import ConfigModule


class ConfigModuleImpl(ConfigModule):
    __config: str
    __global_config: GlobalConfig

    def __init__(self, config: str):
        self.__config = config

    def get_g_hunt_module_config(self) -> GHuntModuleConfig:
        return self.get_global_config().g_hunt_module_config

    def get_company_parser_config(self) -> CompanyParserConfig:
        return self.get_global_config().company_parser_config

    def __set_global_config(self, global_config: GlobalConfig) -> None:
        self.__global_config = global_config

    def get_global_config(self) -> GlobalConfig:
        if not hasattr(self, '__global_config'):
            global_config_data: dict = json.loads(self.__config)
            g_hunt_module_config = self.__get_g_hunt_module_config(global_config_data)
            company_parser_config = self.__get_company_parser_config(global_config_data)
            self.__set_global_config(
                GlobalConfig(
                    g_hunt_module_config=g_hunt_module_config,
                    company_parser_config=company_parser_config
                )
            )
        return self.__global_config

    @staticmethod
    def __get_g_hunt_module_config(global_config_data: dict):
        g_hunt_module_config_data = global_config_data.get('GHuntModuleConfig')
        if g_hunt_module_config_data:
            g_hunt_module_config = GHuntModuleConfig(
                cookies=g_hunt_module_config_data.get('cookies'),
                headers=g_hunt_module_config_data.get('headers'),
                google_docs_token=g_hunt_module_config_data.get('google_docs_token'),
                internal_api_token=g_hunt_module_config_data.get('internal_api_token'),
                internal_api_authorization=g_hunt_module_config_data.get('internal_api_authorization'),
                hangouts_api_authorization=g_hunt_module_config_data.get('hangouts_api_authorization'),
                hangouts_token=g_hunt_module_config_data.get('hangouts_token'),
                ytb_hunt_always=g_hunt_module_config_data.get('ytb_hunt_always'),
            )
        else:
            raise Exception("Config error")
        return g_hunt_module_config

    @staticmethod
    def __get_company_parser_config(global_config_data) -> CompanyParserConfig:
        company_parser_config_data = global_config_data.get('CompanyParser')
        if company_parser_config_data:
            company_parser_config = CompanyParserConfig(
                username=company_parser_config_data.get('username'),
                password=company_parser_config_data.get('password'),
            )
        else:
            raise Exception("Config error")
        return company_parser_config
