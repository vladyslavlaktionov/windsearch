import sys

from maltego_trx.handler import handle_run
from maltego_trx.registry import register_transform_classes
from maltego_trx.server import app

import ui

register_transform_classes(ui)

handle_run(__name__, sys.argv, app)
