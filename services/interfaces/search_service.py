import abc
from typing import List

from models.mail_services_result_bl_model import MailRuResultBLModel, MailServicesResultBLModel, GoogleInfoBLModel


class SearchService(abc.ABC):
    @abc.abstractmethod
    def get_phone_list_by_ukr_net_email(self, email: str) -> List[str]:
        pass

    @abc.abstractmethod
    def get_phone_list_and_email_list_by_mail_ru_email(self, email: str) -> MailRuResultBLModel:
        pass

    @abc.abstractmethod
    def check_email_by_exits_in_mail_services(self, email: str) -> MailServicesResultBLModel:
        pass

    @abc.abstractmethod
    def get_google_info_list_by_gmail_email(self, email: str) -> List[GoogleInfoBLModel]:
        pass
