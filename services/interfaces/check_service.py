import abc
from typing import Optional, List

from models.result_bl_model import CompanyBLModel


class CheckService(abc.ABC):

    @abc.abstractmethod
    def find_company(self, search_query: str) -> List[CompanyBLModel]:
        pass

    @abc.abstractmethod
    def get_company_transaction_info(self, company_id: int, company_name: str,
                                     limit: Optional[int] = 12,
                                     offset: Optional[int] = 0) -> str:
        pass
