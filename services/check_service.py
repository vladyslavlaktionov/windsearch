from typing import Optional, List

from main_dependency.interfaces.module_manager import ModuleManager
from models.result_bl_model import CompanyBLModel
from services.interfaces.check_service import CheckService


class CheckServiceImpl(CheckService):

    def __init__(self,
                 module_manager: ModuleManager
                 ):
        self.__company_parser_module = module_manager.get_company_parser_module()
        self.__telegraph_module = module_manager.get_telegraph_module()

    def find_company(self, search_query: str) -> List[CompanyBLModel]:
        company_result = self.__company_parser_module.search_company(search_query)
        company_list: List[CompanyBLModel] = list()
        for company in company_result.company_list:
            company_list.append(
                CompanyBLModel(
                    id=company.id,
                    name=company.name,
                )
            )

        return company_list

    def get_company_transaction_info(self, company_id: int, company_name: str,
                                     limit: Optional[int] = 20,
                                     offset: Optional[int] = 0) -> str:
        company_transaction = self.__company_parser_module.get_company_transaction_info(company_id)
        text = '<h1>{}</h1><br><br>'.format(company_name)
        if not company_transaction.next_page:
            text += "<strong>Transaction 1</strong><br><br>"
            text += company_transaction.transaction_info.replace("\n", '<br>')
        count = 0
        while company_transaction.next_page and offset <= count < limit:
            text += "<strong>Transaction {}</strong><br><br>".format(company_transaction.next_page - 1)
            text += company_transaction.transaction_info.replace("\n", '<br>')
            company_transaction = self.__company_parser_module.get_company_transaction_info(
                company_id,
                company_transaction.next_page
            )
            text += '<br>' * 2
            count += 1
        return self.__telegraph_module.create_post(
            title=company_name,
            author='Elon Musk',
            text=text,
            author_url='https://www.spacex.com/'
        )
