from typing import List

from exceptions.search_service_exceptions import RateLimitException, EmailNotFoundException
from main_dependency.interfaces.module_manager import ModuleManager
from models.mail_services_result_bl_model import MailRuResultBLModel, MailServicesResultBLModel, GoogleInfoBLModel, \
    CalendarEventBLModel, YoutubeChannelBLModel
from modules.fake_user_agent_module.models import BrowserModel
from modules.interfaces.g_hunt_module import GHuntModule
from modules.ukr_net_api_module.exceptions import RateLimitException as UkrNetApiRateLimitException
from modules.ukr_net_api_module.exceptions import UserNotFoundException as UkrNetApiUserNotFoundException
from services.interfaces.search_service import SearchService


class SearchServiceImpl(SearchService):

    def __init__(self, module_manager: ModuleManager):
        self.ukr_net_api = module_manager.get_ukr_net_api()
        self.mail_service_exists_checker_module = module_manager.get_mail_service_exists_checker_module()
        self.fake_user_agent = module_manager.get_fake_user_agent_module()
        self.g_hunt_module: GHuntModule = module_manager.get_g_hunt_module()

    def get_phone_list_by_ukr_net_email(self, email: str) -> List[str]:
        try:
            request_id = self.ukr_net_api.get_request_id()
            recovery_session_id = self.ukr_net_api.get_recovery_session_id(email, request_id)
            numbers = self.ukr_net_api.get_recovery_info(recovery_session_id)
            return numbers
        except UkrNetApiRateLimitException:
            raise RateLimitException
        except UkrNetApiUserNotFoundException:
            raise EmailNotFoundException

    def check_email_by_exits_in_mail_services(self, email: str) -> MailServicesResultBLModel:
        google_email = self.mail_service_exists_checker_module.check_google(
            email,
            self.fake_user_agent.get_fake_user_agent(BrowserModel.FIREFOX)
        )
        yahoo = self.mail_service_exists_checker_module.check_yahoo(
            email,
            self.fake_user_agent.get_fake_user_agent(BrowserModel.FIREFOX)
        )
        laposte = self.mail_service_exists_checker_module.check_laposte(
            email,
            self.fake_user_agent.get_fake_user_agent(BrowserModel.FIREFOX)
        )
        try:
            mail_ru_result_bl_model = self.get_phone_list_and_email_list_by_mail_ru_email(email)
        except EmailNotFoundException:
            mail_ru_result_bl_model = None
        try:
            ukr_net_phone_list = self.get_phone_list_by_ukr_net_email(
                email,
            )
        except RateLimitException:
            ukr_net_phone_list = []
        except EmailNotFoundException:
            ukr_net_phone_list = []

        protonmail = self.mail_service_exists_checker_module.check_protonmail(
            email,
        )
        return MailServicesResultBLModel(
            google_email=google_email,
            laposte=laposte,
            yahoo=yahoo,
            ukr_net_phone_list=ukr_net_phone_list,
            mail_ru=mail_ru_result_bl_model,
            protonmail=protonmail
        )

    def get_phone_list_and_email_list_by_mail_ru_email(self, email: str) -> MailRuResultBLModel:
        mail_ru_result = self.mail_service_exists_checker_module.check_mail_ru(
            email,
            self.fake_user_agent.get_fake_user_agent(BrowserModel.FIREFOX)
        )
        if mail_ru_result:
            return MailRuResultBLModel(
                phones=mail_ru_result.phones,
                emails=mail_ru_result.emails
            )
        if not mail_ru_result:
            raise EmailNotFoundException

    def get_google_info_list_by_gmail_email(self, email: str) -> List[GoogleInfoBLModel]:
        gmail_info_list = self.g_hunt_module.get_gmail_info_list_by_email(
            email,
        )
        google_info_list: List[GoogleInfoBLModel] = []
        for gmail_info in gmail_info_list:
            google_info_list.append(GoogleInfoBLModel(
                account_id=gmail_info.account_data.account_id,
                name=gmail_info.account_data.name,
                profile_pic_url=gmail_info.account_data.profile_pic_url,
                calendar_events=[
                    CalendarEventBLModel(
                        title=calendar_event.title,
                        date=calendar_event.date,
                        duration=calendar_event.duration
                    ) for calendar_event in gmail_info.calendar_events
                ],
                youtube_channels=[
                    YoutubeChannelBLModel(
                        profile_url=youtube_channel.profile_url,
                        name=youtube_channel.name,
                        avatar_link=youtube_channel.avatar_link,
                        username=youtube_channel.username,
                    ) for youtube_channel in gmail_info.youtube_channels
                ],
                services=gmail_info.services,
                is_bot=gmail_info.is_bot,
                last_edit=gmail_info.last_edit
            ))
        return google_info_list
