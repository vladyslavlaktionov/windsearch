from typing import Optional


class RateLimitException(Exception):
    def __init__(self, message: Optional[str] = None):
        if not message:
            self.message = 'Rate limit exception'
        else:
            self.message = message


class EmailNotFoundException(Exception):
    def __init__(self, message: Optional[str] = None):
        if not message:
            self.message = 'Email not found'
        else:
            self.message = message
