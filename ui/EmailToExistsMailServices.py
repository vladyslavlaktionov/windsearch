from maltego_trx.entities import Website, PhoneNumber, Email
from maltego_trx.maltego import MaltegoEntity
from maltego_trx.transform import DiscoverableTransform

from main_dependency.main_dependency import get_main_dependency
from models.mail_services_result_bl_model import MailServicesResultBLModel


class EmailToExistsMailServices(DiscoverableTransform):
    """
    Receive email from the client, and resolve to exists mail services.
    """
    """
    /Users/vladyslavlaktionov/PycharmProjects/windSearch/venv/bin/python3
    
    project.py local emailtoexistsmailservices
    
    /Users/vladyslavlaktionov/PycharmProjects/windSearch/
    """

    @classmethod
    def create_entities(cls, request, response):
        email = request.Value
        mail_services_result: MailServicesResultBLModel = get_main_dependency().get_service_manager().get_search_service().check_email_by_exits_in_mail_services(email)
        if mail_services_result.protonmail:
            entity = MaltegoEntity(Website, 'https://protonmail.com')
            entity.setNote("Found")
            response.entities.append(entity)
        if mail_services_result.google_email:
            entity = MaltegoEntity(Website, 'https://google.com')
            entity.setNote("Found")
            response.entities.append(entity)
            response.addEntity(Email, mail_services_result.google_email)
        if mail_services_result.yahoo:
            entity = MaltegoEntity(Website, 'https://yahoo.com')
            entity.setNote("Found")
            response.entities.append(entity)
        if mail_services_result.laposte:
            entity = MaltegoEntity(Website, 'https://www.laposte.fr')
            entity.setNote("Found")
            response.entities.append(entity)
        if mail_services_result.ukr_net_phone_list:
            entity = MaltegoEntity(Website, 'https://ukr.net')
            entity.setNote("Found")
            response.entities.append(entity)
        if mail_services_result.mail_ru:
            entity = MaltegoEntity(Website, 'https://mail.ru')
            entity.setNote("Found")
            response.entities.append(entity)
            for phone_number in mail_services_result.mail_ru.phones:
                response.addEntity(PhoneNumber, phone_number)
            for email in mail_services_result.mail_ru.emails:
                response.addEntity(Email, email)
