from typing import List
from maltego_trx.entities import Alias, UniqueID, Phrase, Image, URL, Person
from maltego_trx.transform import DiscoverableTransform

from main_dependency.main_dependency import get_main_dependency
from models.mail_services_result_bl_model import GoogleInfoBLModel


class GoogleEmailToDetail(DiscoverableTransform):
    """
    Receive email from the client, and resolve google details.
    """

    @classmethod
    def create_entities(cls, request, response):
        email = request.Value
        google_info_list: List[
            GoogleInfoBLModel] = get_main_dependency().get_service_manager().get_search_service().get_google_info_list_by_gmail_email(
            email
        )
        for google_info in google_info_list:
            response.addEntity(Alias, google_info.name)
            person_entity = response.addEntity(Person, google_info.name)
            person_entity.setIconURL(google_info.profile_pic_url)
            image_entity = response.addEntity(Image)
            image_entity.addProperty('description', 'Description', 'loose', 'Фото профиля')
            image_entity.addProperty('short-title', 'Short title', 'loose', 'Фото профиля')
            image_entity.addProperty('url', 'URL', 'loose', google_info.profile_pic_url)
            image_entity.setIconURL(google_info.profile_pic_url)
            response.addEntity(UniqueID, google_info.account_id)
            response.addEntity(Phrase, 'Последнее обновление: {}'.format(
                google_info.last_edit.strftime("%m/%d/%Y, %H:%M:%S")
            ))
            for service in google_info.services:
                response.addEntity(Phrase, service)
            for youtube_channel in google_info.youtube_channels:
                url_entity = response.addEntity(URL, youtube_channel.name)
                url_entity.addProperty('title', 'Title', 'loose', youtube_channel.name)
                url_entity.addProperty('url', 'URL', 'loose', youtube_channel.profile_url)
                url_entity.setIconURL(youtube_channel.avatar_link)
