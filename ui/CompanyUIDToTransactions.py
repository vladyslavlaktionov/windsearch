from maltego_trx.entities import URL
from maltego_trx.maltego import UIM_FATAL
from maltego_trx.transform import DiscoverableTransform

from main_dependency.main_dependency import get_main_dependency


class CompanyUIDToTransactions(DiscoverableTransform):
    """
    Receive Company UID from the client, and resolve to transactions.
    """

    @classmethod
    def create_entities(cls, request, response):
        company_uid = request.Value
        try:
            company_id, company_name = str(company_uid).split('_')
            company_id = int(company_id)
        except:
            return response.addUIMessage("Bad Company UID", UIM_FATAL)
        company_transaction_info_link: str = get_main_dependency().get_service_manager().get_check_service() \
            .get_company_transaction_info(
            company_id=company_id, company_name=company_name
        )
        url_entity = response.addEntity(URL)
        url_entity.addProperty('title', 'Title', 'loose', 'Company transactions')
        url_entity.addProperty('short-title', 'Short Title', 'loose', 'Company transactions')
        url_entity.addProperty('url', 'URL', 'loose', company_transaction_info_link)
