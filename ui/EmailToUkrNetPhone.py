from maltego_trx.entities import PhoneNumber
from maltego_trx.maltego import UIM_FATAL
from maltego_trx.transform import DiscoverableTransform

from exceptions.search_service_exceptions import RateLimitException, EmailNotFoundException
from main_dependency.main_dependency import get_main_dependency


class EmailToUkrNetPhone(DiscoverableTransform):
    """
    Receive email from the client, and resolve to phone number.
    """

    @classmethod
    def create_entities(cls, request, response):
        email = request.Value
        try:
            phone_number_list = get_main_dependency().get_service_manager().get_search_service() \
                .get_phone_list_by_ukr_net_email(email)
            for phone_number in phone_number_list:
                response.addEntity(PhoneNumber, phone_number)
        except RateLimitException:
            response.addUIMessage("Rate limit exception", UIM_FATAL)
        except EmailNotFoundException:
            response.addUIMessage("Email not found exception", UIM_FATAL)
