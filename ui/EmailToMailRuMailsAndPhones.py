from maltego_trx.entities import PhoneNumber, Website, Email
from maltego_trx.maltego import UIM_FATAL, MaltegoEntity
from maltego_trx.transform import DiscoverableTransform

from exceptions.search_service_exceptions import RateLimitException, EmailNotFoundException
from main_dependency.main_dependency import get_main_dependency


class EmailToMailRuMailsAndPhones(DiscoverableTransform):
    """
    Receive email from the client, and resolve to phone numbers and mails.
    """

    @classmethod
    def create_entities(cls, request, response):
        email = request.Value
        try:
            mail_ru_result_bl_model = get_main_dependency().get_service_manager().get_search_service() \
                .get_phone_list_and_email_list_by_mail_ru_email(email)
            if mail_ru_result_bl_model:
                entity = MaltegoEntity(Website, 'https://mail.ru')
                entity.setNote("Found")
                response.entities.append(entity)
                for phone_number in mail_ru_result_bl_model.phones:
                    response.addEntity(PhoneNumber, phone_number)
                for email in mail_ru_result_bl_model.emails:
                    response.addEntity(Email, email)
        except RateLimitException:
            response.addUIMessage("Rate limit exception", UIM_FATAL)
        except EmailNotFoundException:
            response.addUIMessage("Email not found exception", UIM_FATAL)
