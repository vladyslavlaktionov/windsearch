from typing import List

from maltego_trx.entities import UniqueID
from maltego_trx.maltego import MaltegoEntity
from maltego_trx.transform import DiscoverableTransform

from main_dependency.main_dependency import get_main_dependency
from models.result_bl_model import CompanyBLModel


class PhraseToCompany(DiscoverableTransform):
    """
    Receive phrase from the client, and resolve to company.
    """

    @classmethod
    def create_entities(cls, request, response):
        search_query = request.Value
        company_list: List[CompanyBLModel] = get_main_dependency().get_service_manager().get_check_service() \
            .find_company(
            search_query=search_query
        )
        for company in company_list:
            entity = MaltegoEntity(UniqueID, "{}_{}".format(company.id, company.name))
            entity.setNote(company.name)
            response.entities.append(entity)
