from dataclasses import dataclass, field
from typing import Optional, List


@dataclass
class CompanyBLModel:
    id: int
    name: str


@dataclass
class SearchCompanyResultBLModel:
    id: Optional[int] = None
    next_page: Optional[int] = None
    previous: Optional[int] = None
    page_count: Optional[int] = None
    company_list: List[CompanyBLModel] = field(default_factory=list)


@dataclass
class SearchResultBLModel:
    company_id: int
    company_name: str
    search_id: int
    id: Optional[int] = None
