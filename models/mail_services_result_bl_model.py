from dataclasses import dataclass, field
from datetime import datetime
from typing import List, Optional

from dateutil.relativedelta import relativedelta


@dataclass
class MailRuResultBLModel:
    phones: List[str] = field(default_factory=list)
    emails: List[str] = field(default_factory=list)


@dataclass
class MailServicesResultBLModel:
    laposte: bool
    yahoo: bool
    google_email: Optional[str] = None
    protonmail: Optional[datetime] = None
    mail_ru: Optional[MailRuResultBLModel] = None
    ukr_net_phone_list: List[str] = field(default_factory=list)

@dataclass
class CalendarEventBLModel:
    title: str
    date: str
    duration: relativedelta


@dataclass
class YoutubeChannelBLModel:
    profile_url: str
    name: str
    avatar_link: str
    username: Optional[str] = None


@dataclass
class GoogleInfoBLModel:
    account_id: int
    name: str
    profile_pic_url: Optional[str]
    calendar_events: List[CalendarEventBLModel]
    services: List[str] = field(default_factory=list)
    youtube_channels: List[YoutubeChannelBLModel] = field(default_factory=list)
    is_bot: Optional[bool] = None
    last_edit: Optional[datetime] = None
